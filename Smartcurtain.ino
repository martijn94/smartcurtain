// this constant won't change.  It's the pin number
// of the sensor's output:
#include <Servo.h> 
const int pin = 7;
Servo myservo;

void setup() {
  // initialize serial communication:
  Serial.begin(9600);
  myservo.attach(9);
  
}

void loop()
{

  long light;
  
  pinMode(pin, OUTPUT);
  digitalWrite(pin, LOW);
  delayMicroseconds(2);
  digitalWrite(pin, HIGH);
  delayMicroseconds(5);
  digitalWrite(pin, LOW);

  pinMode(pin, INPUT);
  //light = pulseIn(pin, HIGH);
  light = analogRead(A0);
  
  if(light < 100) {
    
    myservo.write(80);
    delay(2000); 
    myservo.write(94);
    Serial.print(light);

    Serial.println();
    
  }
  else {
    myservo.write(94);
  }
  
  
 
  delay(1000);
}
